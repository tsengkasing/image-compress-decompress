#include <iostream>
#include <stdio.h>
#include <string>
#include <iomanip>
#include <cstring>

#include "convert.h"
#include "huffman.h"

using namespace std;

int width = 3;
int height = 3;

/*
 * pInput = new Array(27).fill(0).map(i => Math.round(Math.random() * 256));
 */
/* Fake Image Input */
unsigned char *pInput = new unsigned char[27]{
    58, 131, 122, 8, 166, 64, 72, 251, 229, 175, 36, 96, 53, 255, 151, 56, 85, 147, 45, 2, 202, 239, 234, 89, 231, 140, 35
};


string str_bin(unsigned char);
void writeData(unsigned char* &target, int& size, int pos, unsigned char data);
int calcComplementLength(int);
unsigned char *Compress(int &);
void Decompress(unsigned char *compressedData, int cDataSize, unsigned char *uncompressedData);

int main() {

    int cDataSize = width * height * 3;

    cout << "Input Pixels: " << endl;
    for (int i = 0; i < cDataSize; ++i) {
        cout << (unsigned int)pInput[i] << ' ';
    }
    cout << endl << endl;

    // cout << str_bin((unsigned char)0b000111) << endl;
    // printf("0o%x\n", (1 << 3) + 1);
    unsigned char* compressedData = Compress(cDataSize);
    cout << "=============== Compress End ====================\n cDataSize: " << cDataSize << endl;
    cout << "=============== Decompress Begin ================\n" << endl;
    unsigned char *uncompressedData = new unsigned char[cDataSize];
    memset(uncompressedData, 0, sizeof(unsigned char) * cDataSize);
    Decompress(compressedData, cDataSize, uncompressedData);

    cout << "Output Pixels: " << endl;
    for (int i = 0; i < width * height * 3; ++i) {
        cout << (unsigned int)uncompressedData[i] << ' ';
    }
    cout << endl << endl;

    cout << "Input Pixels: " << endl;
    for (int i = 0; i < width * height * 3; ++i) {
        cout << (unsigned int)pInput[i] << ' ';
    }
    cout << endl << endl;


    delete uncompressedData;
    delete compressedData;
    delete pInput;
    return 0;
}

unsigned char *Compress(int &cDataSize) {
    int size_compressed_data = 10;
    unsigned char *compressedData = new unsigned char[size_compressed_data];
    memset(compressedData, 0, sizeof(unsigned char) * size_compressed_data);
    /* Store the position where we writing to @var{compressedData} */
    int pos_data = 1;

    /*
     * huffman = new Array(256).fill(-1);
     * pInput.forEach(v => { huffman[v] = Math.round(Math.random() * 256) });
     * huffman.join(', ');
     */
    unsigned char* huffmanTree = new unsigned char[256 * 32];
    memset(huffmanTree, 0, sizeof(unsigned char) * 256 * 32);

    /*
     * bitLen = new Array(256).fill(0);
     * pInput.forEach(v => { bitLen[v] = Math.round(Math.random() * 8) + 1 });
     * bitLen.join(', ');
     */
    int *encoding_len = new int[256];
    memset(encoding_len, 0, sizeof(int) * 256);

    /* Build Huffman Tree */
    unsigned int encoding_max_len = DictBuild(pInput, cDataSize, huffmanTree, encoding_len);
    encoding_max_len = calcComplementLength(encoding_max_len);
    int byte_encoding_max = encoding_max_len / 8;

    /**
     * Package the Huffman Table into @var{compressedData}
     */
    unsigned int treeSize = 0;
    for (unsigned int i = 0; i < 256; ++i) {
        if (encoding_len[i] != 0) {
            ++treeSize;
            // Put the key of Huffman Table
            writeData(compressedData, size_compressed_data, ++pos_data, (unsigned char)i);
            // Put the value of Huffman Table
            for (int j = 32 - byte_encoding_max; j < 32; ++j) {
                writeData(compressedData, size_compressed_data, ++pos_data, huffmanTree[i * 32 + j] & 0xFF);
            }
            // Put the encoding len of each Huffman code
            writeData(compressedData, size_compressed_data, ++pos_data, (unsigned char)encoding_len[i]);

            // -----------------------
            // printout
            printf("[%3d]", i);
            for (int j = 32 - byte_encoding_max; j < 32; ++j) {
                cout << setfill('0') << setw(8) << str_bin(huffmanTree[i * 32 + j] & 0xFF);
            }
            printf("{%2d}", encoding_len[i]);
            cout << endl;
            // -----------------------
        }
    }
    /**
     * Store the size of Huffman Table
     * &
     * bytes of max encoding len of Huffman Table
     * at the head @var{compressedData}
     */
    compressedData[0] = (unsigned char)treeSize;
    compressedData[1] = (unsigned char)byte_encoding_max;

    cout << "Huffman Tree Size: " << treeSize << endl;
    cout << "Encoding Max Size: " << encoding_max_len << endl;
    cout << "Byte of Encoding Max Size: " << byte_encoding_max << endl;


    // Create Encoded Sequence
    unsigned char *encodedSequence = new unsigned char[cDataSize * byte_encoding_max];
    memset(encodedSequence, 0, sizeof(unsigned char) * cDataSize * byte_encoding_max);
    unsigned int *encodedLength = new unsigned int[cDataSize * byte_encoding_max];
    memset(encodedLength, 0, sizeof(unsigned int) * cDataSize * byte_encoding_max);

    // Create a char*[32] to store a huffman code
    unsigned char* huffmanCode = new unsigned char[32];
    unsigned char* encoded = new unsigned char[byte_encoding_max];

    cout << endl << "============================ Begin Encoding ============================" << endl << endl;
    // pmy => primary, means one of the three-primary-color, like (R, G, B)
    for (int pmy = 0; pmy < cDataSize; ++pmy) {
        // Empty chars
        memset(encoded, 0, sizeof(unsigned char) * byte_encoding_max);
        // Get mapped Huffman Code
        for (int i = 0; i < 32; ++i) {
            huffmanCode[i] = huffmanTree[(pInput[pmy] * 32) + i] & 0xFF;
        }

        // Get mappedHuffman Code Length
        int len = encoding_len[pInput[pmy]];
        for (int i = 31, j = byte_encoding_max, _len = len; _len > 0 && i >= 0 && j >= 0; --i) {
            unsigned int l = _len > 8 ? 8 : _len;
            _len -= l;
            encoded[--j] = huffmanCode[i] & ((1 << l) - 1);
            // Save the encoded bits
            encodedSequence[pmy * byte_encoding_max + j] = encoded[j];
            // Save the length of encoded bits
            encodedLength[pmy * byte_encoding_max + j] = l;
        }

        printf("Encoding: [index]{%2d}[len]{%d}[primaryColor]{%3d}\nHuffmanCode\n", pmy, len, pInput[pmy]);
        for (int i = 32 - byte_encoding_max; i < 32; ++i) {
            cout << setfill('0') << setw(len) << str_bin(huffmanCode[i]) << ' ';
            if ((i + 1) % 4 == 0) putchar('\n');
        }
        cout << "Encoded: " << endl;
        for (int i = 0; i < byte_encoding_max; ++i) {
            cout << setfill('0') << setw(len) << str_bin(encoded[i]) << ' ';
            if ((i + 1) % 4 == 0) putchar('\n');
        }
        cout << endl << endl;

    }
    cout << endl << "============================ End Encoding ============================" << endl;

    for (int i = 0; i < cDataSize; ++i) {
        printf("[%d]\t", encodedLength[i]);
        for (int j = 0; j < byte_encoding_max; ++j) {
            cout << setw(encodedLength[i]) << str_bin(encodedSequence[i * byte_encoding_max + j]) << ' ';
        }
        putchar('\n');
    }
    cout << endl << endl;

    // Free Memory of Huffman
    delete[] huffmanTree;
    delete[] huffmanCode;
    delete[] encoded;

    // Using a Buffer to store the bits before getting 8 bits
    unsigned char buf;

    /* Current bits stored in Buffer */
    unsigned int pos_buf = 0;

    /* Current pointing Position of encodedSequence */
    unsigned int pos_seq = 0;

    unsigned char code = '\0';
    unsigned int len = 0;
    unsigned char code_pending = '\0';
    int seat_pending = -1;

    // Write to @var{CompressedData} from @var{encodedSequence}
    while (pos_buf < 8 || pos_seq < (cDataSize * byte_encoding_max) || seat_pending > -1) {
        // Set Buffer position to -1
        pos_buf = 0;
        // Make Buffer Empty
        buf = 0b0;
        // Loading bits to Buffer until fullfilled it
        while (pos_buf < 8) {
            // Check whether there is some bits pending
            if (seat_pending > -1) {
                code = code_pending;
                len = seat_pending;

                seat_pending = -1;
            } else if (pos_seq >= cDataSize) {
                // Finsihed reading @var{encodedSequence]
                pos_buf = 8;
                break;
            } else {
                code = encodedSequence[pos_seq];
                len = encodedLength[pos_seq];
                ++pos_seq;
            }

            if (pos_buf + len <= 7) {
                /* calculate how many bit to move */
                int bit_to_move = 9 - (pos_buf + 1) - len;

                buf = buf | (code << bit_to_move);
                pos_buf += len;
            } else {
                /* calculate how many seat have in the Buffer */
                int empty_seat = 8 - pos_buf;

                // divide the code into two parts
                // one push into Buffer
                // the other will be using at next iteration
                seat_pending = len - empty_seat;
                buf = buf | (code >> seat_pending);
                code_pending = code & ((1 << seat_pending) - 1);
                pos_buf += empty_seat;
            }
        }
        cout << "Buffer: ";
        cout << setfill('0') << setw (8) << str_bin(buf) << endl;
        writeData(compressedData, size_compressed_data, ++pos_data, buf & 0xFF);
    }

    cout << endl;
    for (int i = 0; i <= pos_data; ++i) {
        printf("[%2d]", i);
        cout << setfill('0') << setw (8) << str_bin(compressedData[i]) << '\t';
        if (i == 1 + treeSize * (1 + byte_encoding_max)) cout << " | end of table | ";
        if ((i + 1) % 4 == 0) cout << endl;
    }

    cout << endl;
    cDataSize = pos_data + 1;

    // Free Memory Space
    delete [] encodedSequence;
    delete [] encodedLength;
    delete[] encoding_len;

    return compressedData;
}

void Decompress(unsigned char *compressedData, int cDataSize, unsigned char *uncompressedData) {
    // Writing position of uncompressedData
    unsigned int pos_uncompressed_data = -1;
    // Reading position of compressedData
    int pos_data = -1;
    // Huffman Tree Table Size
    int treeSize = compressedData[++pos_data] & 0xFF;
    // Maximun Length of Encoding
    int byte_encoding_max = compressedData[++pos_data] & 0xFF;

    printf("Tree Size: %d\nBytes of Encoding: %d\n\n", treeSize, byte_encoding_max);

    unsigned char* huffmanTree = new unsigned char[256 * 32];
    memset(huffmanTree, 0, sizeof(unsigned char) * 256 * 32);
    int *encoding_len = new int[256];
    memset(encoding_len, 0, sizeof(unsigned int) * 256);
    for (int i = 0; i < treeSize && pos_data < cDataSize; ++i) {
        // Recover the key of Huffman Table
        unsigned int key = compressedData[++pos_data] & 0xFF;
        // Recover the value of Huffman Table
        for (int j = 32 - byte_encoding_max; j < 32; ++j) {
            huffmanTree[key * 32 + j] = (compressedData[++pos_data] & 0xFF);
        }
        // Recover the encoding length of Huffman Table
        encoding_len[key] = (int)(compressedData[++pos_data] & 0xFF);
    }

    // -----------------------
    // Printout Huffman Table
    for (unsigned int i = 0; i < 256; ++i) {
        if (encoding_len[i] != 0) {
            printf("[%3d]", i);
            for (int j = 32 - byte_encoding_max; j < 32; ++j) {
                cout << setfill('0') << setw(8) << str_bin(huffmanTree[i * 32 + j] & 0xFF);
            }
            printf("{%2d}", encoding_len[i]);
            cout << endl;
        }
    }
    // -----------------------

    // Rebuild Huffman Tree
    treeNode* rootNode = treeBuild(huffmanTree, encoding_len);

    unsigned char buf = compressedData[++pos_data] & 0xFF;
    unsigned int pos_buf = 0;
    while (pos_data < cDataSize) {
        treeNode* node = rootNode;
        while (1) {
            if (pos_buf > 7) {
                buf = compressedData[++pos_data] & 0xFF;
                pos_buf = 0;
            }
            if ((buf >> (7 - pos_buf)) & 0b1 == 1) {
                if (node->leftChild != nullptr) {
                    node = node->leftChild;
                } else {
                    break;
                }
            } else {
                if (node->rightChild != nullptr) {
                    node = node->rightChild;
                } else {
                    break;
                }
            }
            ++pos_buf;
        }
        // Write to uncompressedData
        uncompressedData[++pos_uncompressed_data] = node->key & 0xFF;
    }

    printf("Decompressed Size: %d\n", pos_uncompressed_data + 1);

    // Free Memory
    delete [] huffmanTree;
    delete [] encoding_len;
}

/**
 * Tools
 */
string str_bin(unsigned char c) {
    unsigned int n = (unsigned int)c;
    convert converter;
    return converter.to_bin(n);
}

/**
 * Calculate the length that complement binary bit
 * examples:
 *       5 ->  8
 *       9 -> 16
 *      17 -> 32
 */
int calcComplementLength(int len) {
    int n = 0;
    while (++n <= 32) {
        if (len < n * 8) return n * 8;
    }
    return 256;
}

void writeData(unsigned char* &target, int& size, int pos, unsigned char data) {
    if (pos + 1 > size) {
        unsigned char* base = target;
        int adjustedSize = int(size * 1.1);
        base = new unsigned char[adjustedSize];
        memcpy(base, target, size);
        size = adjustedSize;
        target = base;
        printf("\n[MEMORY] Allocated to %d\n", adjustedSize);
    }
    target[pos] = data;
}
