//Convert definition
#include "convert.h"
#include <iostream>
#include <string>

std::string convert::int_to_bin(unsigned int number){
    level++;
    if (number > 0){
        result += (number % 2 == 0) ? "0" : "1";
        int_to_bin(number >> 1);
        level--;
    }
    if (level == 1) return reverse(result);
    return result;
}

std::string convert::reverse(std::string to_reverse){
    std::string _result;
    for (int i = to_reverse.length() -1; i >=0 ; i--)
        _result += to_reverse[i];
    return _result;
}

std::string convert::to_bin(unsigned int to_convert){
    level = 0;
    result = "";
    return int_to_bin(to_convert);
}
