#include <string>

#ifndef __CONVERT_H__
#define __CONVERT_H__

class convert{
    public:
        std::string to_bin(unsigned int);
    protected:
        std::string int_to_bin(unsigned int);
        std::string reverse(std::string);
    private:
        std::string result;
        int level;
};

#endif
