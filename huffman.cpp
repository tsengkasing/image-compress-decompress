#include <iostream>
#include <cstring>
#include "huffman.h"
#include "convert.h"
using namespace std;

void InsertNode(treeNode** node, treeNode* newNode) {
    for (int i = 2; i < 256; i++) {
        if (newNode->value < node[i]->value || node[i]->value == 0) {
            for (int j = 2; j < i; j++) {
                node[j - 2] = node[j];
            }
            node[i - 2] = newNode;
            for (int j = i - 1; j < 256; j++) {
                node[j] = node[j + 1];
            }
        }
    }
}

void select(treeNode** node, int* i1, int* i2, int n) {
    for (int i = 0; i < n; i++) {
        if (node[i]->parent == NULL) {
            if (*i1 == 0) {
                *i1 = i;
            }
            else if (*i2 == 0) {
                *i2 = i;
            }
            else {
                break;
            }
        }
    }

    if (node[*i1]->value > node[*i2]->value) {
        int temp = *i1;
        *i1 = *i2;
        *i2 = temp;
    }

    for (int i = 0; i < n; i++) {
        if (node[i]->parent == NULL) {

            if (node[i]->value < node[*i2]->value && i != *i1) {
                if (node[i]->value < node[*i1]->value) {
                    *i2 = *i1; *i1 = i;
                }
                else {
                    *i2 = i;
                }
            }
        }
    }
}

void buildHuffmanTree(treeNode** node, int n) {
    int i1 = 0;
    int i2 = 0;

    select(node, &i1, &i2, n);

    node[i1]->parent = node[n];
    node[i2]->parent = node[n];
    node[n]->leftChild = node[i1];
    node[n]->rightChild = node[i2];
    node[n]->value = node[i1]->value + node[i2]->value;

    //TRACE("node[%d]: %d + node[%d]: %d = node[%d]: %d\r\n", i1, node[i1]->value, i2, node[i2]->value, n, node[n]->value);

}

int DictBuild(unsigned char* pInput, int cDataSize, unsigned char* dict, int* lens) {

    int colorValueShown[257];	// the times of every color shown
    treeNode* node[512];

    //init all values
    for (int i = 0; i < 257; i++) {
        colorValueShown[i] = 0;
        lens[i] = 0;
    }

    for (int i = 0; i < 512; i++) {
        treeNode* tempNode = new treeNode;
        tempNode->value = 0;
        tempNode->parent = NULL;
        tempNode->leftChild = NULL;
        tempNode->rightChild = NULL;
        node[i] = tempNode;
    }

    //calculate the times of every color shown
    for (int i = 0; i < cDataSize; i++) {
        unsigned char colorValue = pInput[i];
        int iColorValue = (unsigned int)colorValue;
        if (iColorValue < 0 || iColorValue >= 256) {
        }
        colorValueShown[iColorValue]++;
    }


    //build all leaf nodes
    int leafNum = 0;
    for (int i = 0; i < 256; i++) {
        if (colorValueShown[i] != 0) {
            node[leafNum]->key = i;
            node[leafNum]->value = colorValueShown[i];
            node[leafNum]->parent = NULL;
            node[leafNum]->leftChild = NULL;
            node[leafNum]->rightChild = NULL;
            leafNum++;
        }
    }

    //build the huffman tree
    for (int i = leafNum; i < 2 * leafNum - 1; i++) {
        buildHuffmanTree(node, i);
    }

    cout << (new convert())->to_bin(dict[0]) << endl;

    int max_len = 0;
    for (int i = 0; i < leafNum; i++) {
        int numParent = 0;
        //int code = 0;
        unsigned char code[32];
        for (int i = 0; i < 32; i++) {
            code[i] = '\0';
        }
        treeNode* parentNode = node[i]->parent;
        treeNode* childNode = node[i];
        while (childNode->parent != NULL) {
            if (childNode == parentNode->leftChild) {
                //code += pow(2, numParent);
                unsigned char temp = code[31-numParent / 8] | (('1' - '0') << (numParent % 8));
                code[31 - numParent / 8] = temp;
            }
            numParent++;
            childNode = childNode->parent;
            parentNode = childNode->parent;

        }

        //dict[node[i]->key] = code;
        memcpy(dict + node[i]->key * 32, code, 32);
        lens[node[i]->key] = numParent;

        if (numParent > max_len) {
            max_len = numParent;
        }
    }
    
    return max_len;

}

treeNode* treeBuild(unsigned char* dict, int* lens) {

    treeNode* nodes[512];
    for (int i = 0; i < 512; i++) {
        treeNode* tempNode = new treeNode;
        tempNode->key = -1;
        tempNode->parent = NULL;
        tempNode->leftChild = NULL;
        tempNode->rightChild = NULL;
        nodes[i] = tempNode;
    }
    int currentNum = 0;
    treeNode* rootNode = nodes[0];
    treeNode* currentNode = rootNode;
    for (int i = 0; i < 256; i++) {
        if (lens[i] != 0) {
            int len = lens[i];
            for (int j = 32 * 8 - len; j < 32 * 8; j++) {
                if ((dict[32 * i + j / 8] >> (7 - j % 8)) & ('1'-'0') == ('1'-'0') ) {
                    if (currentNode->leftChild == NULL) {
                        currentNode->leftChild = nodes[currentNum + 1];
                        currentNum = currentNum + 1;
                    }
                    currentNode = currentNode->leftChild;
                }
                else {
                    if (currentNode->rightChild == NULL) {
                        currentNode->rightChild = nodes[currentNum + 1];
                        currentNum = currentNum + 1;
                    }
                    currentNode = currentNode->rightChild;
                }
            }
            currentNode->key = i;
            currentNode = rootNode;
        }
    }

    return rootNode;
}

/*
int main(int argc, char* argv) {

    unsigned char *pInput = new unsigned char[27]{
      58, 131, 122, 8, 166, 64, 72, 251, 229, 175, 36, 96, 53, 255, 151, 56, 85, 147, 45, 2, 202, 239, 234, 89, 231, 140, 35
    };

    int cDataSize = 27;

    unsigned char dict[256*32];
    for (int i = 0; i < 256 * 32; i++) {
        dict[i] = '0'-'0';
    }
    int *lens = new int[256];

    int max_len = DictBuild(pInput, cDataSize, dict, lens);

    cout << (new convert())->to_bin(dict[32* 202 +31]) << endl;
    cout<< lens[202] << endl;





    treeNode* rootNode = treeBuild(dict, lens);

    unsigned char ch = '\0';
    ch = ch | ('1' - '0');
    ch = (ch << 1) | ('0' - '0');
    cout<< (new convert())->to_bin(ch) << endl;

    treeNode* node = rootNode;
    for (int i = 0; i < lens[175]; i++) {
        if ((ch >> 4 - i) & ('1' - '0') == 1) {
            node = node->leftChild;
        }
        else(node = node->rightChild);
    }
    cout << node->key << endl;


    
    cout << max_len << endl;

    system("pause");
    return 0;
}*/
