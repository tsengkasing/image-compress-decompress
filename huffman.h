/* Huffman Algorithm */
struct treeNode {
    int key;
    int value;
    treeNode* parent;
    treeNode* leftChild;
    treeNode* rightChild;
};

void InsertNode(treeNode**, treeNode*);
void select(treeNode**, int*, int*, int);
void buildHuffmanTree(treeNode**, int);
int DictBuild(unsigned char*, int, unsigned char*, int*);
treeNode* treeBuild(unsigned char*, int*);
